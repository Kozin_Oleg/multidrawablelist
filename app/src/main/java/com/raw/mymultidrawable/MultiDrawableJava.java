package com.raw.mymultidrawable;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

/**
 * @author RAW: zp.dv.kom@gmail.com
 * @since 4/27/18
 */

public class MultiDrawableJava extends Drawable {

    private final int mCenterLength;

    private ImageView mViewById;

    private int mImageSize;

    private List<Drawable> mDrawables;

    public MultiDrawableJava(int ImageSize, ImageView viewById, List<Drawable> profilePhotos, int centerLength) {

        mCenterLength = centerLength;

        mImageSize = ImageSize;

        mViewById = viewById;

        mDrawables = profilePhotos;

        viewById.setImageDrawable(this);

        ViewGroup.LayoutParams layoutParams = viewById.getLayoutParams();

        layoutParams.width = ((profilePhotos.size() + 1) * mImageSize / 2) + ((profilePhotos.size() - 1) * mCenterLength);
        layoutParams.height = mImageSize;

        viewById.setLayoutParams(layoutParams);

        setBounds(0, 0, mImageSize, mImageSize);
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        for (int i = mDrawables.size() - 1; i >= 0; i--) {
            canvas.save();
            mDrawables.get(i).setBounds(0, 0, mImageSize, mImageSize);
            canvas.clipRect(0, 0, mViewById.getWidth(), mViewById.getHeight());
            canvas.translate(mViewById.getHeight() / 2 * i + (mCenterLength * i), 0);
            mDrawables.get(i).draw(canvas);
            canvas.restore();
        }
    }

    @Override
    public void setBounds(@NonNull Rect bounds) {
        super.setBounds(bounds);
    }


    @Override
    public void setAlpha(int i) {

    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {

    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

}
