package com.raw.mymultidrawable;

import java.util.ArrayList;

/**
 * @author RAW: zp.dv.kom@gmail.com
 * @since 4/27/18
 */

public class GroupAvatarModel {

    //    private ArrayList<Drawable> mImages;
    private ArrayList<String> mImageUrls;

    public void add(String url) {
        if (mImageUrls == null) {
            mImageUrls = new ArrayList<>();
        }

        mImageUrls.add(url);
    }

    public ArrayList<String> get() {
        return mImageUrls;
    }
}
