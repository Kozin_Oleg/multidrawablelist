package com.raw.mymultidrawable;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import static com.raw.mymultidrawable.ImageUtility.setGroupImage;

public class MainActivity extends AppCompatActivity {

    public static final int AVATAR_IMAGE_SIZE = 200;
    public static final int ACCORDION_SIZE = 20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        GroupAvatarModel groupAvatarModel = new GroupAvatarModel();

        groupAvatarModel.add("https://inigoappdata.blob.core.windows.net/filespace/ProfilePictures/3e2d31e9-8444-4ca5-bc71-f82f84127646_cropped-82021880.jpg");
        groupAvatarModel.add("https://inigoappdata.blob.core.windows.net/filespace/ProfilePictures/3e2d31e9-8444-4ca5-bc71-f82f84127646_cropped-82021880.jpg");
        groupAvatarModel.add("https://inigoappdata.blob.core.windows.net/filespace/ProfilePictures/3e2d31e9-8444-4ca5-bc71-f82f84127646_cropped-82021880.jpg");
        groupAvatarModel.add("https://inigoappdata.blob.core.windows.net/filespace/ProfilePictures/3e2d31e9-8444-4ca5-bc71-f82f84127646_cropped-82021880.jpg");
        groupAvatarModel.add("https://inigoappdata.blob.core.windows.net/filespace/ProfilePictures/3e2d31e9-8444-4ca5-bc71-f82f84127646_cropped-82021880.jpg");
        groupAvatarModel.add("https://inigoappdata.blob.core.windows.net/filespace/ProfilePictures/3e2d31e9-8444-4ca5-bc71-f82f84127646_cropped-82021880.jpg");
        groupAvatarModel.add("https://inigoappdata.blob.core.windows.net/filespace/ProfilePictures/3e2d31e9-8444-4ca5-bc71-f82f84127646_cropped-82021880.jpg");
        groupAvatarModel.add("https://inigoappdata.blob.core.windows.net/filespace/ProfilePictures/3e2d31e9-8444-4ca5-bc71-f82f84127646_cropped-82021880.jpg");

        setGroupImage(this, (ImageView) findViewById(R.id.asdsad), groupAvatarModel, AVATAR_IMAGE_SIZE, ACCORDION_SIZE);
    }
}
