package com.raw.mymultidrawable;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @author raw <zp.dv.kom@gmail.com>
 * @since 11/3/16
 */

public class ImageUtility {

    private static final int ERROR_IMAGE = R.mipmap.ic_launcher;
    private static final int FALLBACK_IMAGE = R.mipmap.ic_launcher;

    public static void setGroupImage(final Activity activity, final ImageView viewById, final GroupAvatarModel groupAvatarModel, final int avatarImageSize, final int accordionSize) {
        if (groupAvatarModel.get() != null)
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {

                    try {
                        final List<Drawable> profilePhotos = new ArrayList<>();
                        for (String url : groupAvatarModel.get()) {

                            RequestManager with = Glide.with(activity);

                            profilePhotos.add(with.load(url)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .skipMemoryCache(false)
                                    .error(ERROR_IMAGE)
                                    .fallback(FALLBACK_IMAGE)
                                    .bitmapTransform(
                                            new CenterCrop(activity),
                                            new RoundedCornersTransformation(activity, avatarImageSize / 2, 0, RoundedCornersTransformation.CornerType.ALL))
                                    .override(avatarImageSize, avatarImageSize)
                                    .into(avatarImageSize, avatarImageSize).get());
                        }

                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    new MultiDrawableJava(avatarImageSize, viewById, profilePhotos, accordionSize);
                                } catch (Throwable e) {
                                    //do nothing
                                }
                            }
                        });
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
    }
}